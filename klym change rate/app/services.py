from app.db.session import get_session
from fastapi import Request
from dataclass import ProductCreate, ProductResponse
from sqlalchemy.orm import Session

from provider import get_today_change_rated

from db.models import Product


def create_product(product: dict, db: Session = get_session()) -> ProductCreate:

    db_product = Product(**product.dict())

    db.add(db_product)
    db.commit()
    db.refresh(db_product)


    return ProductCreate(
        id=db_product.id,
        title=db_product.title,
        price=db_product.price,
        created_at=db_product.created_at,
    )
    


def update_product(product: ProductCreate):
    newsletter = db_session.query(Newsletter).filter(Newsletter.id == newsletter_id).first()

    return product

def currency_change(price: float, change_rate: float) -> float:
    return price * change_rate


def get_products_from_db(currency: str) -> list[ProductResponse]:
    products = db_session.query(Product).all()
    change_rate = get_today_change_rated(currency)

    list_response = []
    for i in products:
        new_price = currency_change(i.price, change_rate)
        _product = ProductResponse(
            id=i.id,
            title=i.title,
            price=new_price,
            created_at=i.created_at,
        )
        list_response.append(_product)

    return list_response    

    
    
    

    