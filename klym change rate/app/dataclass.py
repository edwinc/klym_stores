from pydantic import BaseModel

class ProductCreate(BaseModel):
    tittle: str
    price: float


class ProductResponse(ProductCreate):
    id: int