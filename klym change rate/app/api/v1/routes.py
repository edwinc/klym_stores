from fastapi import APIRouter, HTTPException, Body, Path

from services import create_product, ProductCreate, ProductResponse, update_product, get_products_from_db

router = APIRouter()

@router.post("/product", response_model=ProductResponse)
async def create_new_product(product: ProductCreate = Body(...)):
    product = create_product(product)
    if product:
        return product
    raise HTTPException(status_code=400, detail="Product not created")

@router.put("/product", response_model=ProductResponse)
async def update_existent_product(product: ProductCreate = Body(...)):
    product = update_product(product)
    if product:
        return product
    raise HTTPException(status_code=400, detail="Product not created")




@router.get("/product", response_model=ProductResponse)
async def get_products(currency: str) -> list[ProductResponse]:

    product = get_products_from_db(currency)

    # pagination

    return product