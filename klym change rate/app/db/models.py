from datetime import datetime

from app.db.base import Base
from sqlalchemy import Column, Integer, String, Float, DateTime


class Product(Base):
    __tablename__ = "products"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    price = Column(Float) 
    created_at = Column(DateTime, default=datetime.utcnow)