from config import config
import requests
import cache



class ExecptionApi(Exception):
    pass


def get_today_change_rated(currency: str):

    url_change_rate_api = config.URL_CHANGE_RATE_API
    url = f'{url_change_rate_api}/USD/to/{currency} '
    response = requests.get(url)


    cache_key = F"change_rate_{currency}"

    if cache_key in cache.cache:
        return cache.cache[cache_key]
    

    if response.status_code == 200:
        today_currency = response.json()["rates"][currency]
        cache.set("change_rate", today_currency, 60*60*24)
        return today_currency
    
    else:
        raise ExecptionApi("Service not available")
    