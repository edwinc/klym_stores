import httpx
import pytest
from datetime import datetime
from main import app


url_base = "http://127.0.0.1:8000"  


test_produt = {
    "title": "Teste",
    "price": 10.0,
}

@pytest.mark.asyncio
async def test_create_product():
    async with httpx.AsyncClient(app=app, base_url=url_base) as client:
        # TODO
        response = await client.post("/product/", json=test_produt)
        assert response.status_code == 200
        data = response.json()
        assert data["title"] == test_produt["title"]
        assert data["price"] == test_produt["price"]


@pytest.mark.asyncio
async def test_create_product():
    async with httpx.AsyncClient(app=app, base_url=url_base) as client:
        
        # Create new product and edit 
        price = 20
        response = await client.post("/product/", json=test_produt)
        assert response.status_code == 200
        data = response.json()
        assert data["title"] == test_produt["title"]
        assert data["price"] == test_produt["price"]