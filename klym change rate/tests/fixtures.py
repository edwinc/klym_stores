from db.models import Product
import factory, faker

@pytest.fixture
def product():
    return Product(
        title=factory.LazyAttribute(lambda _: faker.name()),
        price=10.0,
    ) 

@pytest.fixture
def products():
    return [product() for _ in range(5)]